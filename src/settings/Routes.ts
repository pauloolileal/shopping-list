import React from 'react';
import Home from "../pages/Home/Home";

const Routes = [
    {
        path: "home",
        element: Home
    }
];

export default Routes;
