import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import {
  Button,
  Col,
  Container,
  Form,
  FormControl,
  InputGroup,
  Row,
} from "react-bootstrap";
import Icon from "../../../components/Icon/Icon";
import SelectSearch from "../../../components/SelectSearch/SelectSearch";
import "./CreateVariant.css";

const CreateVariant = () => (
  <>
    <Form>
      <Container>
        <Row>
          <Col>
            <SelectSearch
              name="Brand"
              labelField="name"
              valueField="brandId"
              values={brandValues}
              onChange={(value: any) => console.log(value)}
            />
          </Col>
          <Col>
            <SelectSearch
              name="Model"
              labelField="name"
              valueField="modelId"
              values={modelValues}
              onChange={(value: any) => console.log(value)}
            />
          </Col>
        </Row>

        <Row>
          <Col xs={12} md={6}>
            <Form.Label>Barcode</Form.Label>
            <InputGroup>
              <Form.Control type="number" placeholder="Barcode" />
              <Button variant="outline-secondary" className="barcode-button">
                <Icon name="barcode" />
              </Button>
            </InputGroup>
          </Col>
          <Col xs={12} md={6}>
            <Form.Label>Price</Form.Label>
            <InputGroup >
              <InputGroup.Text>R$</InputGroup.Text>
              <FormControl type="number" placeholder="Price"/>
            </InputGroup>
          </Col>
        </Row>
      </Container>

      <div className="button-bar">
        <Button variant="outline-danger" type="submit">
          Cancel
        </Button>
        <Button variant="primary" type="submit">
          Add
        </Button>
      </div>
    </Form>
  </>
);

const brandValues = [
  { brandId: 1, name: "Dove" },
  { brandId: 2, name: "Seda" },
];

const modelValues = [
  { brandId: 1, name: "Coco" },
  { brandId: 2, name: "Creme" },
];

export default CreateVariant;
