import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import CreateVariant from './CreateVariant';

describe('<CreateVariant />', () => {
  test('it should mount', () => {
    render(<CreateVariant />);
    
    const createVariant = screen.getByTestId('CreateVariant');

    expect(createVariant).toBeInTheDocument();
  });
});