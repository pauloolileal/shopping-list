import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import ListVariant from './ListVariant';

describe('<ListVariant />', () => {
  test('it should mount', () => {
    render(<ListVariant />);
    
    const listVariant = screen.getByTestId('ListVariant');

    expect(listVariant).toBeInTheDocument();
  });
});