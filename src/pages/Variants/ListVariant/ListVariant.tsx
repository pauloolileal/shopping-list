import React from 'react';
import './ListVariant.css';

const ListVariant = () => (
  <div className="ListVariant" data-testid="ListVariant">
    ListVariant Component
  </div>
);

export default ListVariant;
