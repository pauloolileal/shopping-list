import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import ListCart from './ListCart';

describe('<ListCart />', () => {
  test('it should mount', () => {
    render(<ListCart />);
    
    const listCart = screen.getByTestId('ListCart');

    expect(listCart).toBeInTheDocument();
  });
});