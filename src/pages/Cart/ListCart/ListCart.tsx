import React, { useState } from "react";
import Icon from "../../../components/Icon/Icon";
import { Image } from "react-bootstrap";
import "./ListCart.css";
import { IconName } from "@fortawesome/fontawesome-common-types";
import CartItem from "../../../components/Cart/CartItem/CartItem";

function ListCart() {
  return (
    <>
      <CartItem />
      <CartItem />
      <CartItem />
    </>
  );
}

export default ListCart;
