import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import ListProduct from './ListProduct';

describe('<ListProduct />', () => {
  test('it should mount', () => {
    render(<ListProduct />);
    
    const listProduct = screen.getByTestId('ListProduct');

    expect(listProduct).toBeInTheDocument();
  });
});