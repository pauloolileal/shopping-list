import React from "react";
import { Form, Button } from "react-bootstrap";
import Select from "react-dropdown-select";
import SelectSearch from "../../../components/SelectSearch/SelectSearch";
import "./CreateProduct.css";

const CreateProduct = () => (
  <Form>
    <Form.Group className="mb-3" controlId="formCreateProduct">
      <Form.Label>Product name</Form.Label>
      <Form.Control type="text" placeholder="Enter product name" />
    </Form.Group>

    <SelectSearch 
      name="Category"
      labelField="name"
      valueField="categoryId"
      values={values}
      onChange={(value:any) => console.log(value)}
    />

    <Form.Group className="mb-3" controlId="formCreateFrigde">
      <Form.Check type="checkbox" label="Go to the fridge?" />
    </Form.Group>

    <div className="button-bar">
      <Button variant="outline-danger" type="submit">
        Cancel
      </Button>
      <Button variant="primary" type="submit">
        Add
      </Button>
    </div>
  </Form>
);

const values = [
  { categoryId: 1, name: "Padaria" },
  { categoryId: 2, name: "Açougue" },
];

export default CreateProduct;
