import React from "react";
import {
  Navbar,
  Container,
  Nav,
  NavDropdown,
  Form,
  FormControl,
  Button,
} from "react-bootstrap";
import Icon from "../Icon/Icon";
import ShoppingButton from "../ShoppingButton/ShoppingButton";
import "./Sidebar.css";

const Sidebar = () => (
  <Navbar bg="light" expand="lg">
    <Container fluid>
      <Navbar.Brand href="#">Shopping List</Navbar.Brand>
      <Navbar.Toggle aria-controls="navbarScroll" />
      <Navbar.Collapse id="navbarScroll">
        <Nav
          className="me-auto my-2 my-lg-0"
          style={{ maxHeight: "100px" }}
          navbarScroll
        >
          <Nav.Link href="/home">Home</Nav.Link>
          {/* <Nav.Link href="/products">Products</Nav.Link> */}
          <NavDropdown title="Products" id="navbarScrollingDropdown">
            <NavDropdown.Item href="/products">List</NavDropdown.Item>
            <NavDropdown.Item href="/products/create">
              Register product
            </NavDropdown.Item>
          </NavDropdown>
          <NavDropdown title="Variants" id="navbarScrollingDropdown">
            <NavDropdown.Item href="/variants">List</NavDropdown.Item>
            <NavDropdown.Item href="/variants/create">
              Register product
            </NavDropdown.Item>
          </NavDropdown>
          {/* <NavDropdown.Divider />
          <NavDropdown.Item href="#action5">
            Something else here
          </NavDropdown.Item> */}
          {/* <Nav.Link href="#" disabled>
          Link
        </Nav.Link> */}
        </Nav>
        <ShoppingButton/>
      </Navbar.Collapse>
    </Container>
  </Navbar>
);

export default Sidebar;
