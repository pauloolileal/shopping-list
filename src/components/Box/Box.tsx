import React from 'react';
import './Box.css';

const Box = () => (
  <div className="Box" data-testid="Box">
    Box Component
  </div>
);

export default Box;
