import React, { useState } from "react";
import { Button } from "react-bootstrap";
import Icon from "../Icon/Icon";
import "./ShoppingButton.css";

function ShoppingButton() {
  const [cartTotal, updateCartTotal] = useState(0);

  return (
    <div className="shoopingButton">
      <div className="shoppingButtonInfo">
        <span>R$ {cartTotal}</span>
        <span>10/20</span>
      </div>
      <Button
        variant="outline-info"
        className="shoppingButtonCart"
        href="/cart"
      >
        <Icon name="shopping-cart" />
      </Button>
    </div>
  );
}

export default ShoppingButton;
