import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import ShoppingButton from './ShoppingButton';

describe('<ShoppingButton />', () => {
  test('it should mount', () => {
    render(<ShoppingButton />);
    
    const shoppingButton = screen.getByTestId('ShoppingButton');

    expect(shoppingButton).toBeInTheDocument();
  });
});