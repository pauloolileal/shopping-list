import React from "react";
import { IconName, IconPrefix } from "@fortawesome/fontawesome-common-types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";
import { far } from "@fortawesome/free-regular-svg-icons";
import { fab } from "@fortawesome/free-brands-svg-icons";
import "./Icon.css";

export interface IconProps {
  type?: "solid" | "regular";
  name: IconName;
}

library.add(fas);
library.add(far);
library.add(fab);

function GetType(type: any): IconPrefix {
  switch (type) {
    case "regular":
      return "far";

    case "brand":
      return "fab";

    default:
      return "fas";
  }
}

const Icon = (props: IconProps) => (
  <FontAwesomeIcon icon={[GetType(props.type), props.name]} />
);

export default Icon;
