import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import CartItemVariant from './CartItemVariant';

describe('<CartItemVariant />', () => {
  test('it should mount', () => {
    render(<CartItemVariant />);
    
    const cartItemVariant = screen.getByTestId('CartItemVariant');

    expect(cartItemVariant).toBeInTheDocument();
  });
});