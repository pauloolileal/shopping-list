import React, { useEffect, useState } from "react";
import CurrencyInput from "react-currency-input-field";
import Icon from "../../Icon/Icon";
import "./CartItemVariant.css";

export interface CartItemVariantProps {
  index: number;
  isNew?: boolean | false;
  onClick: () => void;
  onChange: (data: ItemVariant) => void;
}

export interface ItemVariant {
  index: number;
  countItens: number;
  brand: string;
  model: string;
  price: number;
  barcodeNumber: number;
}

const CartItemVariant = (prop: CartItemVariantProps) => {
  const [item, updateItem] = useState<ItemVariant>({
    index: prop.index,
    countItens: 0,
    brand: "Brand",
    model: "Model",
    price: 0,
    barcodeNumber: 0,
  });

  var isNew = prop.isNew;

  useEffect(() => {
    prop.onChange(item);
  }, [item]);

  return (
    <div className="cartItemVariant">
      <div className="cartItemVariantAction" onClick={prop.onClick}>
        {!isNew ? (
          <Icon name="times-circle" type="regular" />
        ) : (
          <Icon name="plus-circle" />
        )}
      </div>

      <div className="cartItemVariantBox">
        <div className="cartItemVariantCount">
          <input
            className="h4 inputBorderless"
            disabled={isNew}
            value={item.countItens}
            onChange={(e) =>
              updateItem({ ...item, countItens: Number(e.target.value) })
            }
          />
        </div>

        <div className="cartItemVariantInfo">
          <div className="cartItemVariantBrand">
            <input
              className="inputBorderless"
              disabled={isNew}
              value={item.brand}
              onChange={(e) => updateItem({ ...item, brand: e.target.value })}
            />
          </div>

          <div className="cartItemVariantModel">
            <input
              className="inputBorderless"
              disabled={isNew}
              value={item.model}
              onChange={(e) => updateItem({ ...item, model: e.target.value })}
            />
          </div>
        </div>

        <div className="cartItemVariantPrice">
          <span>R$</span>
          <CurrencyInput
            className="inputBorderless"
            allowDecimals
            allowNegativeValue={false}
            disabled={isNew}
            decimalsLimit={2}
            fixedDecimalLength={2}
            decimalSeparator=","
            groupSeparator="."
            value={item.price}
            onValueChange={(value, name) => updateItem({ ...item, price: Number(value) })}
          />
        </div>

        <div className="cartItemVariantBarcode">
          <Icon name="barcode" />
        </div>
      </div>
    </div>
  );
};

export default CartItemVariant;
