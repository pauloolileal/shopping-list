import React, { useState } from "react";
import Icon from "../../../components/Icon/Icon";
import { Image } from "react-bootstrap";
import { IconName } from "@fortawesome/fontawesome-common-types";
import "./CartItem.css";
import CartItemVariant, {
  ItemVariant,
} from "../CartItemVariant/CartItemVariant";

var itemVariantId = 0;

function CartItem() {
  const [isExpanded, updateExpandIcon] = useState(false);
  const [itemComponents, setItemComponents] = useState<any[]>([]);
  const [itemList, setItemList] = useState<ItemVariant[]>([]);

  function addItemVariant() {
    itemVariantId++;
    let clone = [...itemComponents];

    clone.push(
      <CartItemVariant
        index={itemVariantId}
        onClick={() => removeItem(itemVariantId)}
        onChange={(data) => addItemList(data)}
      />
    );

    setItemComponents(clone);
  }

  function removeItem(index: number): void {
    let clone = [...itemComponents];
    clone.splice(index, 1);
    setItemComponents(clone);
    removeItemList(index);
  }

  function addItemList(item: ItemVariant) {
    var clone = [...itemList];
    clone.push(item);
    setItemList(clone);
  }

  function updateItemList(data: ItemVariant) {
    var clone = [...itemList];
    clone.splice(data.index, 1);
    clone.push(data);
    setItemList(clone);
  }

  function removeItemList(index: number): void {
    let clone = [...itemList];
    clone.splice(index, 1);
    setItemList(clone);
  }

  var chevronIcon: IconName = "chevron-down";
  if (isExpanded) chevronIcon = "chevron-up";

  function sumCountItens(){
    var total = itemList.reduce(function(prev, cur) {
      return prev + cur.countItens;
    }, 0);
    return total;
  }

  function sumPriceItens(){
    var total = itemList.reduce(function(prev, cur) {
      return prev + Number(cur.price);
    }, 0);
    return total;
  }

  return (
    <div>
      <div className="cartItem">
        <div
          className="cartItemChevron"
          onClick={() => updateExpandIcon(!isExpanded)}
        >
          <Icon name={chevronIcon} />
        </div>
        <div className="cartItemImage">
          <Image
            src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22171%22%20height%3D%22180%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20171%20180%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_17d5403c941%20text%20%7B%20fill%3A%23999%3Bfont-weight%3Anormal%3Bfont-family%3Avar(--bs-font-sans-serif)%2C%20monospace%3Bfont-size%3A10pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_17d5403c941%22%3E%3Crect%20width%3D%22171%22%20height%3D%22180%22%20fill%3D%22%23373940%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2260.8828125%22%20y%3D%2295.1%22%3E171x180%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E"
            roundedCircle
          />
        </div>

        <div className="cartItemCount">
          <h2>{sumCountItens()}</h2>
        </div>

        <div>
          <span>Sabonete</span>
        </div>

        <div className="cartItemPrice">
          <span>R$ {sumPriceItens()}</span>
        </div>

        <div className="cartItemCheck">
          <Icon name="check" />
        </div>
      </div>

      {isExpanded &&
        itemComponents.map((id, i) => (
          <CartItemVariant
            index={i}
            onClick={() => removeItem(i)}
            onChange={(data) => updateItemList(data)}
          />
        ))}
      {isExpanded && (
        <CartItemVariant
          index={-1}
          isNew
          onClick={() => addItemVariant()}
          onChange={(data) => updateItemList(data)}
        />
      )}
    </div>
  );
}

export default CartItem;
