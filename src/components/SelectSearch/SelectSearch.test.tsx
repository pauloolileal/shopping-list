import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import SelectSearch from './SelectSearch';

describe('<SelectSearch />', () => {
  test('it should mount', () => {
    render(<SelectSearch />);
    
    const selectSearch = screen.getByTestId('SelectSearch');

    expect(selectSearch).toBeInTheDocument();
  });
});