import React from "react";
import Select from "react-dropdown-select";
import "./SelectSearch.css";

export interface SelectSearchProps {
  name: string;
  labelField: string;
  valueField: string;
  values: any[];
  onChange: (value: any) => void;
  onCreate?: (item: any) => void;
}

function onChange(selectedValues: any[], fn: (selected: any) => void) {
  if (selectedValues.length == 0) fn(null);
  else fn(selectedValues[0]);
}

const SelectSearch = (props: SelectSearchProps) => (
  <>
    <p className="select-search-label">{props.name}</p>
    <Select
      className="select-search"
      clearable
      searchable
      create
      sortBy={props.labelField}
      labelField={props.labelField}
      valueField={props.valueField}
      options={props.values}
      values={[]}
      onChange={(values) => onChange(values, props.onChange)}
      onCreateNew={props.onCreate}
    />
  </>
);

export default SelectSearch;
