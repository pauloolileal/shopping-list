import React from "react";
import logo from "./logo.svg";
import "./App.css";
import Home from "./pages/Home/Home";
import { BrowserRouter as Router, Routes, Route, Link } from "react-router-dom";
import Sidebar from "./components/Sidebar/Sidebar";
import ListProduct from "./pages/Product/ListProduct/ListProduct";
import CreateProduct from "./pages/Product/CreateProduct/CreateProduct";
import ListVariant from "./pages/Variants/ListVariant/ListVariant";
import CreateVariant from "./pages/Variants/CreateVariant/CreateVariant";
import ListCart from "./pages/Cart/ListCart/ListCart";

function App() {
  return (
    <>
      <Sidebar />
      <div className="blank-area">
        <Router>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/home" element={<Home />} />
            <Route path="/products" element={<ListProduct />} />
            <Route path="/products/create" element={<CreateProduct />} />
            <Route path="/variants" element={<ListVariant />} />
            <Route path="/variants/create" element={<CreateVariant />} />
            <Route path="/cart/" element={<ListCart />} />
          </Routes>
        </Router>
      </div>
    </>
  );
}

export default App;
